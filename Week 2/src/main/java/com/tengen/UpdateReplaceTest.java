package com.tengen;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class UpdateReplaceTest {

    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("students");
        MongoCollection mongoColl = mongoDb.getCollection("grades");

        mongoColl.drop();

        // insert 10 documents
        for (int i = 0; i < 10; i++) {
            mongoColl.insertOne(new Document()
                    .append("_id", i)
                    .append("x", i));
        }

        //replace
        mongoColl.replaceOne(Filters.eq("x", 5), new Document("_id", 5)
                .append("x", 20)
                .append("update", true));
        //update
        mongoColl.updateOne(Filters.eq("x", 5), new Document("$set",
                new Document("x", 20)));

        //UPSERT
        mongoColl.updateOne(Filters.eq("x", 5), new Document("$set",
                new Document("x", 20)),
                new UpdateOptions().upsert(true));
    }

}
