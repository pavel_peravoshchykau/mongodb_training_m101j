package com.tengen;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.BsonArray;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 * removing from collection lowest homework score
 */
public class HomeTask3_1 {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("school");
        MongoCollection mongoColl = mongoDb.getCollection("students");

        MongoCursor<Document> cursor = mongoColl.find()
                .iterator();

        while (cursor.hasNext()) {

            Document obj = cursor.next();
            List<Document> scores = (List<Document>) obj.get("scores");


            Document newHomeworkScore = null;
//            BsonArray newScores = new BsonArray();
            List<Document> newScores = new ArrayList<Document>();

            for (Document score : scores) {
                if (score.getString("type").equals("homework")){
                    if (newHomeworkScore != null) {
                        if (score.getDouble("score").compareTo(newHomeworkScore.getDouble("score")) > 0 ){
                            newHomeworkScore = score;
                            newScores.add(score);
                        } else {
                            newScores.add(newHomeworkScore);
                        }
                    } else {
                        newHomeworkScore = score;
                    }
                }
                if (!score.getString("type").equals("homework")){
                    newScores.add(score);
                }
            }

            // update record
            mongoColl.findOneAndUpdate(Filters.eq("_id", obj.get("_id")), new Document("$set", new Document("scores", newScores)));
        }
    }
}
