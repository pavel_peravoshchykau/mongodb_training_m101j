package com.tengen;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.BsonDocument;
import org.bson.Document;


/**
 * Written by Pavel Peravoshchykau at 17.08.2015  Time: 22:04
 */
public class _ConfMongoDbClient {

    public static void main(String[] args) {

//        MongoClient client = new MongoClient("localhost", 27017);

        //sending a list of servers
//        MongoClient client = new MongoClient(Arrays.asList(new ServerAddress("localhost", 27017)));

//        MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));

//        MongoClient client = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));

//        MongoClientOptions mongoClientOptions = MongoClientOptions.builder().build();
        MongoClientOptions mongoClientOptions = MongoClientOptions.builder().connectionsPerHost(500).build();
        MongoClient client = new MongoClient(new ServerAddress(), mongoClientOptions);

        MongoDatabase db = client.getDatabase("test").withReadPreference(ReadPreference.secondary());

//        MongoCollection coll = db.getCollection("test");
//        MongoCollection<Document> coll = db.getCollection("test");
        MongoCollection<BsonDocument> coll = db.getCollection("test", BsonDocument.class);

    }
}
