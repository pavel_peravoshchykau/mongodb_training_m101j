package com.tengen;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.tengen.utils.PrintHelper;
import org.bson.Document;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class HomeTask2_3 {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("students");
        MongoCollection mongoColl = mongoDb.getCollection("grades");

        MongoCursor<Document> cursor = mongoColl.find(new Document("type", "homework"))
                .sort(new Document("student_id", 1)
                        .append("score", 1))
                        .iterator();


        Integer student_id = null;
        Integer student_id_temp = null;

        long count = mongoColl.count();
        System.out.println(count);

        while (cursor.hasNext()) {

            Document obj = cursor.next();
            PrintHelper.printJson(obj);
            student_id_temp = obj.getInteger("student_id");

            if (!student_id_temp.equals(student_id)){
                mongoColl.deleteOne(obj);
            }
            student_id = student_id_temp;
        }
    }
}
