package com.tengen;

import com.tengen.utils.PrintHelper;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class _DocRepresenting {

    public static void main(String[] args) {
        Document document = new Document()
                .append("str", "Hello, MongoDb!")
                .append("int", 42)
                .append("l", 1L)
                .append("double", 1.1)
                .append("b", false)
                .append("date", new Date())
                .append("objectId", new ObjectId())
                .append("null", null)
                .append("embedded", new Document("x", 0))
                .append("list", Arrays.asList(1, 2, 3));

        String str = (String) document.getString("str");
        int i = document.getInteger("int");

        PrintHelper.printJson(document);

        // or you can use BsonDocument, but it a little more complicated
        BsonDocument bsonDocument = new BsonDocument("str", new BsonString("Hello MongoDB!"));
    }
}
