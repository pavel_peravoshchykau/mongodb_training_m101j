package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.tengen.utils.PrintHelper;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.List;
import java.util.Random;
import java.util.logging.Filter;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class FindWithFilter_IteratorTest {


    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("students");
        MongoCollection mongoColl = mongoDb.getCollection("grades");

        mongoColl.drop();

        // insert 10 documents with a random integer as the value of field "x"
        for (int i = 0; i < 10; i++) {
            mongoColl.insertOne(new Document()
                    .append("x", new Random().nextInt(2))
                    .append("y", new Random().nextInt(100)));
        }

        Bson filter = new Document("x", 1);

        long count = mongoColl.count(filter);
        System.out.println(count);


        MongoCursor<Document> cursor1 = mongoColl.find().iterator();
        try {
            while (cursor1.hasNext()){
                Document cur = cursor1.next();
                PrintHelper.printJson(cur);
            }
        }   finally {
            cursor1.close();
        }

        /*
        Or multiple filter
         */

        filter = new Document("x", 1)
                .append("y", new Document("$gt", 50)
                .append("$lt", 90));



        //Or using Filters class
        filter = Filters.eq("x", 0);


        count = mongoColl.count(filter);
        System.out.println(count);


        cursor1 = mongoColl.find().iterator();
        try {
            while (cursor1.hasNext()){
                Document cur = cursor1.next();
                PrintHelper.printJson(cur);
            }
        }   finally {
            cursor1.close();
        }


    }
}
