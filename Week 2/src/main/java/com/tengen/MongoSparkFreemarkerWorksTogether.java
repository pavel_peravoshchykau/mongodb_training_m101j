package com.tengen;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.bson.Document;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class MongoSparkFreemarkerWorksTogether {

    public static void main(String[] args) {


        final Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setClassForTemplateLoading(MongoSparkFreemarkerWorksTogether.class, "/");

        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("course");
        final MongoCollection mongoColl = mongoDb.getCollection("hello");

        mongoColl.drop();

        mongoColl.insertOne(new Document("name", "MongoDB"));

        Spark.get(new Route("/") {
            @Override
            public Object handle(Request request, Response response) {

                StringWriter stringWriter = new StringWriter();
                try {
                    Template helloTemplate = configuration.getTemplate("hello.ftl");

                    Document document = (Document) mongoColl.find().first();

                    helloTemplate.process(document, stringWriter);

                } catch (Exception e) {
                    halt(500);
                    e.printStackTrace();
                }
                return stringWriter;
            }
        });
    }
}
