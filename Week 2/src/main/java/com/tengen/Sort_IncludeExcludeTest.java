package com.tengen;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class Sort_IncludeExcludeTest {

    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("students");
        MongoCollection mongoColl = mongoDb.getCollection("grades");

        mongoColl.drop();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                mongoColl.insertOne(new Document()
                        .append("i", i)
                        .append("j", j));
            }
        }

        Bson filter = new Document("x", 1)
                .append("y", new Document("$gt", 50)
                        .append("$lt", 90));

        // 0 means exclude field from results. 1 means include value.
        Bson protection = new Document("x", 0)
                .append("_id", 0);

        // or you can use Projections builder
        protection = Projections.exclude("x", "_id");
        //or
        protection = Projections.include("y", "_id");
        // get y and i, but exclude _id
        protection = Projections.fields(Projections.exclude("y", "i"),
                Projections.exclude("_id"));

        List<Document> all = (List<Document>) mongoColl
                .find(filter)
                .projection(protection)
                .into(new ArrayList<Document>());
    }
}
