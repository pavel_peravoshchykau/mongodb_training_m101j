/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.tengen.utils.PrintHelper;
import org.bson.Document;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FindTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("course");
        DBCollection collection = db.getCollection("findTest");
        collection.drop();

        // insert 10 documents with a random integer as the value of field "x"
        for (int i = 0; i < 10; i++) {
            collection.insert(new BasicDBObject("x", new Random().nextInt(100)));
        }

        System.out.println("Find one:");
        DBObject one = collection.findOne();
        System.out.println(one);

        System.out.println("\nFind all: ");
        DBCursor cursor = collection.find();
        try {
          while (cursor.hasNext()) {
              DBObject cur = cursor.next();
              System.out.println(cur);
          }
        } finally {
            cursor.close();
        }

        System.out.println("\nCount:");
        long count = collection.count();
        System.out.println(count);

        /*
        Another way
         */

        MongoDatabase mongoDb = client.getDatabase("course");
        MongoCollection mongoColl = mongoDb.getCollection("insertTest");

        mongoColl.drop();

        // insert 10 documents
        for (int i = 0; i < 10; i++) {
            mongoColl.insertOne(new Document("x", i));
        }

        System.out.println("Find one:");
        Document first = (Document) mongoColl.find().first();
        PrintHelper.printJson(first);

        System.out.println("Find all with into: ");
        List<Document> all = (List<Document>) mongoColl.find().into(new ArrayList<Document>());
        for (Document document : all) {
            PrintHelper.printJson(document);
        }

        System.out.println("Find all with iterations: ");
        MongoCursor<Document> cursor1 = mongoColl.find().iterator();
        try {
            while (cursor1.hasNext()){
                Document cur = cursor1.next();
                PrintHelper.printJson(cur);
            }
        }   finally {
            cursor1.close();
        }

        System.out.println("Count: ");
        long count1 = mongoColl.count();
        System.out.println(count1);


    }
}
