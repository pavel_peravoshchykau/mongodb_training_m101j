package com.tengen;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

/**
 * Created by Pavel_Peravoshchykau on 8/18/2015.
 */
public class DeleteTest {

    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase mongoDb = client.getDatabase("students");
        MongoCollection mongoColl = mongoDb.getCollection("grades");

        mongoColl.drop();

        // insert 10 documents
        for (int i = 0; i < 10; i++) {
            mongoColl.insertOne(new Document("x", i));
        }

        mongoColl.deleteMany(Filters.gt("_id", 4));
    }

}
