import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class HelloWorldFromSpark {
    public static void main( String[] args ) {
        Spark.get(new Route("/") {
            @Override
            public Object handle(Request request, Response response) {
                return "Hello world from Spark!";
            }
        });

        Spark.get(new Route("/test") {
            @Override
            public Object handle(Request request, Response response) {
                return "This is a test page!";
            }
        });

        Spark.get(new Route("/echo/:value") {
            @Override
            public Object handle(Request request, Response response) {
                return request.params("value");
            }
        });
    }
}
