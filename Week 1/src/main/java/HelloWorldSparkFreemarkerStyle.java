import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pavel_Peravoshchykau on 8/13/2015.
 */
public class HelloWorldSparkFreemarkerStyle {

    public static void main( String[] args ) {

        final Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setClassForTemplateLoading(HelloWorldSparkFreemarkerStyle.class, "/");

        Spark.get(new Route("/") {
            @Override
            public Object handle(Request request, Response response) {

                StringWriter stringWriter = new StringWriter();

                try {
                    Template helloTemplate = configuration.getTemplate("hello.ftl");

                    Map<String, Object> helloMap = new HashMap<String, Object>();
                    helloMap.put("name", "Pavel");

                    helloTemplate.process(helloMap, stringWriter);

                    System.out.println(stringWriter);

                } catch (Exception e) {
                    halt(500);
                    e.printStackTrace();
                }
                return stringWriter;
            }
        });
    }
}


