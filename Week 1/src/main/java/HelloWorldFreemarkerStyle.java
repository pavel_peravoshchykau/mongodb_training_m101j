import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pavel_Peravoshchykau on 8/13/2015.
 */
public class HelloWorldFreemarkerStyle {

    public static void main(String[] args) {

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setClassForTemplateLoading(HelloWorldFreemarkerStyle.class, "/");

        try {
            Template helloTemplate  = configuration.getTemplate("hello.ftl");
            StringWriter stringWriter = new StringWriter();
            Map<String, Object> helloMap = new HashMap<String, Object>();
            helloMap.put("name", "Pavel");

            helloTemplate.process(helloMap, stringWriter);

            System.out.println(stringWriter);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
